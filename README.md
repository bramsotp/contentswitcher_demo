contentswitcher
===============

contentswitcher was created to ease the development of multi-lingual jsPsych
tasks, but can be used without jsPsych too.

Alternative versions of content are referred to here as "contentSets". A
string code (e.g. 'en' or 'fr') selects which contentSet the content is
retrieved from.

	var L = new contentSwitcherV2(['en','fr']);

	L({en: 'Hello', fr: 'Bonjour'})
	-> returns 'Hello'

	L.switch('fr');
	L({en: 'Hello', fr: 'Bonjour'})
	-> returns 'Bonjour'

ContentSets may be used to support different languages, but actually you
can use them for any kind of alternative versions of content, not just
languages.

	var L = new contentSwitcherV2(['verbose','brief']);
	L.switch('brief')
	L({verbose: 'Hello there', brief: 'Hi'})
	-> returns 'Hi'

ContentSets are often used to select different versions of text, but
actually the content can be any type of value supported in JavaScript
(number, object, array, blob, etc).

	buttonOptions = L({en:['Yes', 'No'], fr:['Oui', 'Non']})


To use contentswitcher, include a line in your script to create a
switcher variable:

	var L = new contentSwitcherV2(['english','piglatin'], 'defer');

I call my variable "L" for brevity and to connote "language" since I use
it to switch languages, but you can call it whatever you like.


The first argument is the array of labels corresponding to the languages
(or other content sets) you will be using. The first value in the array is
the one that is selected initially.

If you are switching between languages, consider using standard language
codes for brevity, e.g. "en", "fr", etc. But you can choose whatever makes
sense for you.
https://www.andiamo.co.uk/resources/iso-language-codes/


The second argument is optional. It may be the string "defer", or an object
containing one or more option values, e.g. {mode:"defer"} or {strict:true}.

The "defer" option is what you will want most of time in jsPsych. In "defer"
mode, every time the switcher is used the return value is a function to later
retrieve the content, rather than the content itself. This allows us to
switch the active contentSet between the moment when a particular item is
defined and when it is actually used. This is advantageous in jsPsych because
the timeline is defined before it is run, but we may want to switch the
contentSet while the timeline is running (e.g. based on the user's choice on
a trial).

When the strict option is selected, attempting to retrieve a value via a
contentSet code that does not exist in the current content (or via a content
ID that has not already been defined via the add() method) will throw a
JavaScript error. If strict is not selected, then this situation will return
an undefined value and no error will be thrown.


After you have created your switcher, you can do these things:

	// Switch to use the contentSet "lang2".
	L.switch("lang2")

	// Return the value that matches the current contentSet code.
	L({lang1:"value", lang2:"value"})

	// Store a set of values to an ID that you can use later.
	// This could be useful to move your content definitions into a
	// separate javascript file (e.g. to send to someone to translate).
	L.add(id, {lang1:"value", lang2:"value"})

	// Return a previously stored value by ID, using the current
	// contentSet.
	L(id)

	// Immediately get a value (even if the contentswitcher was created
	// in "defer" mode).
	L.get({lang1:"value", lang2:"value"})
	L.get(id)

	// Get a value as a retrieval function for deferred usage (even if
	// the contentswitcher was created without "defer").
	L.defer({lang1:"value", lang2:"value"})
	L.defer(id)

By Nick Foster 2021